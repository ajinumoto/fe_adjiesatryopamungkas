var nama = "";
var jabatan = "";
var alamat = "";
var email = "";
var phone = "";
var website = "";
var design = "";


function showCard(x) {
    design = x;
    nama = document.getElementById("nama").value;
    jabatan = document.getElementById("jabatan").value;
    alamat = document.getElementById("alamat").value;
    email = document.getElementById("email").value;
    phone = document.getElementById("phone").value;
    website = document.getElementById("website").value;

    console.log(design);
    document.getElementById("card2f").style.display = "none";
    document.getElementById("card2b").style.display = "none";
    document.getElementById("card3f").style.display = "none";
    document.getElementById("card3b").style.display = "none";
    document.getElementById("card4f").style.display = "none";
    document.getElementById("card4b").style.display = "none";

    if (nama == "" || jabatan == "" || alamat == "" || email == "" || phone == "" || website == "") {
        alert("Data harus terisi semua!");
    } else {
        switch (design) {
            case 1:
                console.log("case1");
                showC1();
                break;
            case 2:
                console.log("case2");
                showC2();
                break;
            case 3:
                console.log("case3");
                showC3();                
                break;
            default:
            // code block
        }
    }
}

function showC1() {
    document.getElementById("nama2").innerHTML = nama;
    document.getElementById("jabatan2").innerHTML = jabatan;
    document.getElementById("alamat2").innerHTML = alamat;
    document.getElementById("email2").innerHTML = email;
    document.getElementById("phone2").innerHTML = phone;
    document.getElementById("website2").innerHTML = website;
    document.getElementById("card2f").style.display = "flex";
    document.getElementById("card2b").style.display = "flex";
}

function showC2() {
    document.getElementById("nama3").innerHTML = nama;
    document.getElementById("jabatan3").innerHTML = jabatan;
    document.getElementById("alamat3").innerHTML = alamat;
    document.getElementById("email3").innerHTML = email;
    document.getElementById("phone3").innerHTML = phone;
    document.getElementById("website3").innerHTML = website;
    document.getElementById("card3f").style.display = "flex";
    document.getElementById("card3b").style.display = "flex";
}

function showC3() {
    document.getElementById("nama4").innerHTML = nama;
    document.getElementById("jabatan4").innerHTML = jabatan;
    document.getElementById("alamat4").innerHTML = alamat;
    document.getElementById("email4").innerHTML = email;
    document.getElementById("phone4").innerHTML = phone;
    document.getElementById("website4").innerHTML = website;
    document.getElementById("card4f").style.display = "flex";
    document.getElementById("card4b").style.display = "flex";
}
