/* task5.js */


var n = 0;
var m = 0;
var idEdit = 0;
var rowEdit = 0;
const id = [];
const nama = [];
const umur = [];

function inputFunction(z) {
  let index=n;
  let namevalue = document.getElementById("namev").value;
  let agevalue = document.getElementById("agev").value;
	if (namevalue == "" || agevalue == "") {
		alert("Data harus terisi!");
	} else {
		if (document.getElementById("submitbutton").value == "Edit"){
			nama[idEdit]=namevalue;
			umur[idEdit]=agevalue;
			document.getElementById("submitbutton").value = "Submit";
			editTable();
		}else {
			id.push(n+1);
			nama.push(namevalue);
			umur.push(agevalue);
			tableFunction(index);
			n++;
		}
	}
}

function tableFunction(index) {
	let table = document.getElementById("myTable");
	let row = table.insertRow(n+1-m);
	let cell1 = row.insertCell(0);
	let cell2 = row.insertCell(1);
	let cell3 = row.insertCell(2);
	let cell4 = row.insertCell(3);
	
	cell1.innerHTML = id[n];
	cell2.innerHTML = nama[n];
	cell3.innerHTML = umur[n];
	cell4.innerHTML = '<input type="button" onclick="deleteFunction(this,'+index+')" value="Delete" style="color:red" id="deletebutton"> <input type="button" onclick="editFunction(this,'+index+')" value="Edit" style="font-weight:bold" id="editbutton">';
}

function deleteFunction(x,z) {
	
	console.log(z);	
	if (confirm("Apakah anda yakin akan menghapus data " + nama[z]) == true) {
		let y = x.parentNode.parentNode.rowIndex;
		document.getElementById("myTable").deleteRow(y);
		m++;
	} else {
	}
}

function editFunction(x,z) {
	idEdit=z;
	console.log(nama[z]);
	document.getElementById("namev").value = nama[z];
	document.getElementById("agev").value = umur[z];
	document.getElementById("submitbutton").value = "Edit";
	rowEdit = x.parentNode.parentNode.rowIndex;
}

function editTable(){
	document.getElementById("myTable").deleteRow(rowEdit);
	let row = document.getElementById("myTable").insertRow(rowEdit);

	let cell1 = row.insertCell(0);
	let cell2 = row.insertCell(1);
	let cell3 = row.insertCell(2);
	let cell4 = row.insertCell(3);
	
	cell1.innerHTML = id[idEdit];
	cell2.innerHTML = nama[idEdit];
	cell3.innerHTML = umur[idEdit];
	cell4.innerHTML = '<input type="button" onclick="deleteFunction(this,'+idEdit+')" value="Delete" style="color:red" id="deletebutton"> <input type="button" onclick="editFunction(this,'+idEdit+')" value="Edit" style="font-weight:bold" id="editbutton">';
}
